module.exports = {
  // see https://github.com/iamvishnusankar/next-sitemap
  siteUrl: 'https://blog.spotfix.com.br/',
  generateRobotsTxt: true,
  sitemapSize: 7000,
}
