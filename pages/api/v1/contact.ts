import { NextApiRequest, NextApiResponse } from 'next'

import nodemailer from 'nodemailer'
import validator from 'email-validator'
import sanitize from 'sanitize-html'
import { processEnv } from '@lib/processEnv'

interface SendEmailProps {
  name: string
  email: string
  subject: string
  message: string
}

const smtp = {
  port: 465,
  secure: true,
  tls: { rejectUnauthorized: false },
  debug: false,
  host: 'smtp.gmail.com',
  auth: {
    user: 'comercial@spotfix.com.br',
    pass: 'yxoncptbskqyslrs',
  },
}

const transporter = nodemailer.createTransport(smtp)

const sendEmail = async ({ name, email, subject, message }: SendEmailProps) => {
  const output = `
      <p>Olá,<p>
      <p>Você recebeu uma nova solicitação de contato.</p>
      <h3>Detalhes do Contato</h3>
      <ul><li>Nome: ${name}</li><li>Email: ${email}</li></ul>
      <h3>Menssagem:</h3>
      <p>${message}</p>
  `
  const sendData = {
    from: email,
    to: 'carlos.souza@spotfix.com.br',
    subject: 'Solicitação de Contato SpotFix - ' + ((subject && subject.toUpperCase()) || ''),
    html: sanitize(output),
  }
  return await transporter.sendMail(sendData)
}

const Contact = async (req: NextApiRequest, res: NextApiResponse): Promise<NextApiResponse | void> => {
  const { origin } = req.headers
  const { contactPage, siteUrl } = processEnv

  if (!contactPage) {
    return res.status(404).json({
      error: 404,
      message: 'Endpoint not found.',
    })
  }

  if (origin !== siteUrl) {
    return res.status(400).json({
      error: 400,
      message: 'Wrong origin. Check your siteUrl.',
    })
  }

  if (req.method !== 'POST') {
    return res.status(400).json({
      error: 400,
      message: 'Wrong request method.',
    })
  }

  const { body } = req
  if (!validator.validate(body.email)) {
    return res.status(400).json({
      error: 400,
      message: 'Wrong email.',
    })
  }

  try {
    await sendEmail(body)
  } catch (error) {
    console.warn(error)
    return res.status(404).json({
      error: 404,
      message: 'Sending message failed.',
    })
  }

  res.json({ status: 'ok' })
}

export default Contact
